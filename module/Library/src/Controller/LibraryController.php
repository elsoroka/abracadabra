<?php

namespace Library\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class LibraryController extends AbstractActionController
{
    public function indexAction()
    {
//        return 'Это будет домашняя страница со списком книг';
        return new ViewModel();
    }

    public function addAction()
    {
//        return 'Добавить книгу в библиотеку';
    }

    public function editAction()
    {
//        return 'Изменить существующую книгу в библиотеке';
    }

    public function deleteAction()
    {
//        return 'Удалить книгу из библиотеки';
    }
}