# Abracadabra

Скачать проект и перейти в папку с проектом:

```bash
$ git clone https://elsoroka@bitbucket.org/elsoroka/abracadabra.git
$ cd abracadabra
$ php composer.phar self-update
$ php composer.phar install
```

Запуск проекта:

```bash
$ docker-compose up -d
```
Проект доступен по адресу(локально):
http://127.0.0.10/

Оставить проект:

```bash
$ docker stop $(docker ps -a -q)
```

Чистка образов docker(Опционально):

```bash
$ docker system prune -f --volumes
```